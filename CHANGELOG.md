# SecT thesis template changelog

## Latest Dev
* Henning Seidler

## v0.1.0
* initial version
* basic structure and important packages

## Clean Thesis
This template is based on Clean Thesis v0.4.0
The latest version of this file can be found at the master branch of the
*Clean Thesis* [repository](https://github.com/derric/cleanthesis).